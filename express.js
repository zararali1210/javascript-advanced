
/*----------------------Node Js and Express-----------------------*/

/*
Node Js: Node is open source and cross platform and its runtime virtual virtual environment for executing the jS outside of the browser
We use for back-end service API and web app, mobile app. It writte in JS, C, C++


Express: Express is Node JS framwork, we can't use without Node js. Normally we used for repid development and we can manage routing
etc. We can manage middleware, it can model , controller and service
and it requires the less coding and it written in JS


With Express

let express= require("express");
let app= express();
app.get('/', (req,res)=>{
    console.log(res);
});
app.listen(8080,function()=>{
    console.log("Server Stat")
})


Without Express

var http= require("http")

http.createServer(function(req,res)=>{
    console.log("Server Star");
}).listen(8080);


*/