/*--------------------------------Event LOOOOOP---------------------- */

/*

Evennt loop executing the code,collecting and proccess events, executing queued sub task.

Node: Event Loop is an endless loop, which wait for the task, executes then sleep until it receives more task
The event loop executes tasks from the event queue only when the call stack is empty i.e. 
there is no ongoing task. The event loop allows us to use callbacks and promises.

We have multiple phase which is use in Event loop

1. Request.  Its means incoming request can be blocking or non bloacking,its depends on user what task perform
in web application
2. Event Queue: its take the request or stored the request and pass to event loop and then send back client
3. Event Loop: Event Loop take the request from the queu and process them and send the client whatevent user perform the
opertion in web application
4. Thead Pool: Thread is reponsible for completing the blocking request by accesing the external request
 such a file, database etc
 5. Worker Thread: It will request from thread pool and read it and  then it will display the result


Event Loops Phases
1. Time
2. Pending Callback
3. idle, Prepare
4. Poll
5. Check
6. Close Callback


Timers: Callbacks scheduled by setTimeout() or setInterval() are executed in this phase.
Pending Callbacks: I/O callbacks deferred to the next loop iteration are executed here.
Idle, Prepare: Used internally only.
Poll: Retrieves new I/O events.
Check: It invokes setIntermediate() callbacks.
Close Callbacks: It handles some close callbacks. Eg: socket.on(‘close’, …)


*/


