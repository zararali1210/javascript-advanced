/* Welcome back in sequelize */

/*
Sequelize is an easy-to-use and promise-based Node.js ORM tool for Postgres, MySQL, MariaDB, SQLite, DB2, Microsoft SQL Server, and Snowflake. 
It features solid transaction support, relations, eager and lazy loading, read replication and more.
1. which is responsible for MySql connection
2. which is responsible for defining the model
3. mappings between a model and a table, use the define method.


EX:
Configure Database.js
const Sequelize = require('sequelize')
const sequelize = new Sequelize(
   'YOUR_DB_NAME', // TutorialsPoint
   'YOUR_DB_USER_NAME', // root
   'YOUR_DB_PASSWORD', //root{
      dialect: 'mysql',
      host: 'localhost'
   }
);
module.exports = sequelize


------------------------------

// Importing the database model
const sequelize = require('./database')

// Importing the user model
const User = require('./user')

// Creating all the tables defined in user
sequelize.sync()

// You can change the user.js file
// And run this code to check if it overwrites the existing code.
sequelize.sync({force:true)

*/

