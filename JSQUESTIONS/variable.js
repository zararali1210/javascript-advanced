/*
Const: We can reassigned and we can not declare the variable without a initilized the value
Let: We can not re-declare the variable and but we can re-assign new value
var: We can declare many time and re-assign the value also
*/

// for let

// shadowing scope

var a = 20;
let b = 30;
function x() {
  if (true) {
    var a = 10;
    var b = 50;
    console.log(b);
    console.log(a);
  }
}

x();
