/*-------------------SLICE--------------------------*/
/*
slice(): Slice is JS method where retun selected element in new array. Its does not changes the original array
splice: Splice is JS methhod where we can add /remove the element from the array and retun the deleted element. It override the original array.
*/

/*-----------------------SLICE EXAMPLE------------------------------*/

let arraySlice= [1,2,5,6,3,2,9,0,7]
// console.log(arraySlice.slice(0));// OP: [1,2,5,6,3,2,9,0,7]
// console.log(arraySlice) // OP: [1,2,5,6,3,2,9,0,7]

// console.log(arraySlice.slice(1,2));//   OP: [2]  syntex slice(index,index-1)  
// console.log(arraySlice.slice(2,5)); //  [5,6,3]
// console.log(arraySlice.slice(2,0)); //  first index should be lest than and it will not same value also, otherwise it will retun empty array
// console.log(arraySlice.slice(1,-4)); // [2,5,6,3]
// console.log(arraySlice.slice(-1,-4));// [] if -ve then index will start from 1
// console.log(arraySlice.slice(-5,-3));// [3,2,9] Not include last one
// console.log(arraySlice.slice(2,-4)); // [5,6,3]
// console.log(arraySlice.slice(2,-3)); // [ 5, 6, 3, 2 ]
// console.log(arraySlice.slice(2,-1));//  [ 5, 6, 3, 2, 9, 0 ]
// console.log(arraySlice.slice(1,4)); //  [2,5,6]

//array.splice(index, howmany, item1, ....., itemX)
console.log("=========");
arraySlice.splice(-5,3, "Hi Im'here");
console.log(arraySlice)


var arr = [1,2,3,4,5,6,7,8]; 
console.log(arr.slice(1,3)); 

var cars=['Benz', 'Innova', 'Breeza', 'Etios', 'Dzire']; // 
var new_cars=cars.splice(3,0);
console.log(cars)
// cars.splice(2, 0, 'ambassedor', 'BMW', 'Audi');
// console.log(cars)

// cars.splice(2, 1);
// console.log(cars)

