/*---------DESTRUCTRING--------------------*/
/*
Its bacically unpack the value of array and object properties and stored in distinct varible
1. Its allow us extract the data from object, array, and map and set them into new distinct varibale

*/

const data = {
    "results": [{
        "gender": "female",
        "name": {
            "title": "ms",
            "first": "emily",
            "last": "simmons"
        },
        "location": {
            "street": "1514 preston rd",
            "city": "mackay",
            "state": "victoria",
            "postcode": 3943,
            "coordinates": {
                "latitude": "-82.6428",
                "longitude": "99.3586"
            },
            "timezone": {
                "offset": "-5:00",
                "description": "Eastern Time (US & Canada), Bogota, Lima"
            }
        },
        "email": "emily.simmons@example.com",
        "login": {
            "uuid": "4db43a8c-f811-4f66-9063-8de9af1b7ff4",
            "username": "brownlion857",
            "password": "girls",
            "salt": "Fff9zzxa",
            "md5": "4eb010fc1f3e9f72b6298b75cec001a1",
            "sha1": "086f0a1c0db596967033a77df62e21e6d407f647",
            "sha256": "d7f99aae053957d788fe17a80922877d04a491bd7ea00d7b6b41c94329468e12"
        },
        "dob": {
            "date": "1992-10-20T03:47:03Z",
            "age": 26
        },
        "registered": {
            "date": "2012-02-25T19:05:12Z",
            "age": 7
        },
        "phone": "09-1749-9293",
        "cell": "0490-139-057",
        "id": {
            "name": "TFN",
            "value": "338334455"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/64.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/64.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/64.jpg"
        },
        "nat": "AU"
    },
    {
        "gender": "female",
        "name": {
            "title": "ms",
            "first": "emily",
            "last": "simmons"
        },
        "location": {
            "street": "1514 preston rd",
            "city": "mackay",
            "state": "victoria",
            "postcode": 3943,
            "coordinates": {
                "latitude": "-82.6428",
                "longitude": "99.3586"
            },
            "timezone": {
                "offset": "-5:00",
                "description": "Eastern Time (US & Canada), Bogota, Lima"
            }
        },
        "email": "emily.simmons@example.com",
        "login": {
            "uuid": "4db43a8c-f811-4f66-9063-8de9af1b7ff4",
            "username": "brownlion857",
            "password": "girls",
            "salt": "Fff9zzxa",
            "md5": "4eb010fc1f3e9f72b6298b75cec001a1",
            "sha1": "086f0a1c0db596967033a77df62e21e6d407f647",
            "sha256": "d7f99aae053957d788fe17a80922877d04a491bd7ea00d7b6b41c94329468e12"
        },
        "dob": {
            "date": "1992-10-20T03:47:03Z",
            "age": 12
        },
        "registered": {
            "date": "2012-02-25T19:05:12Z",
            "age": 7
        },
        "phone": "09-1749-9293",
        "cell": "0490-139-057",
        "id": {
            "name": "TFN",
            "value": "338334455"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/64.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/64.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/64.jpg"
        },
        "nat": "AU"
    }],
    "info": {}
}

//const {result: [{dob: {date, age}}]} =data;
const [age]= data.results.map(val=>val.dob.age) // With Josn Object
console.log(age)// op: 26

let obj={
    'fname':'Zarar',
    'lname':'Ali'
}

let {fname,lname}= obj
console.log(fname);

// With Array

let  arrayDs=[1,2,3,4,5,6,{'fname':'Zarar'}];
let [a1,,,,,,name]=arrayDs;
console.log(name.fname);



const date1 = new Date('7/13/2010');
const date2 = new Date('12/15/2010');
const diffTime = Math.abs(date2 - date1);
const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
console.log(diffTime + " milliseconds");
console.log(diffDays + " days");




















