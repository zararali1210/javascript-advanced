/*
1. push();
2. we can add the element into original array 
3. it will changes the oginal array and return the new length
3. it will add the element in the last of array
*/

let array=[1,2,3,4,5,6];
array.push(7,8,9);
console.log(array);
let obj=[{
    a:1,
    b:2
}]
obj.push({
    c:3
});
console.log(obj);

/*
1. pop();
2. we can remove the element from original array 
3. it will changes the oginal array and return the remove element
3. it will remove the element in the last of array
*/

// let lastArry= [1,2,3,4,5,6];
// let finalArray= lastArry.pop();
// console.log(lastArry); // return removed element


/*
1. shift();
2. we can remove the element from original array 
3. it will changes the oginal array
3. it will remove the element in the start of array
*/

// let lastArry= [1,2,3,4,5,6];
// lastArry.shift();
// console.log(lastArry)





/*
1. unshift();
2. we can add the element from original array 
3. it will changes the oginal array
3. it will add the element in the start of array
*/

let lastArry= [1,2,3,4,5,6];
lastArry.unshift("0");
console.log(lastArry)




