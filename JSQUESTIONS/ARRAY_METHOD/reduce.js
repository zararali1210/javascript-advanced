/* Welcome in reduce method */


/*
1. method does not changes the original value
2. Reduce of empty array with no initial value
3. return single value
*/

let array=[0,2,3,6,1];
//let finalReduce= array.reduce((acc,curr)=>acc+curr);
//console.log(finalReduce);
/*-----------------------------------------------------*/
let reduceByFunction= array.reduce(myReduceFunction);
console.log(reduceByFunction)// 7
function myReduceFunction(total,curr){
    console.log(curr)
    return total-curr
}

/*-----------------------------------------------------*/


/*
The Object.valueOf() is a method that returns the primitive value of an object. 
The primitive value is the value of type undefined, null, boolean, number, string, or symbol.

*/

let a=10;
let b= a.valueOf();
console.log(b);