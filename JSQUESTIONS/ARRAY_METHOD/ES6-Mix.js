//1 Default parameters

function defaultFunction(a,b=10){
     return a+b
}

console.log(defaultFunction(10))

/*--------------------------------*/
// Tagged Templates: we can parse template literals with a function

let myName = 'John';
let myRole = 'Software Developer';
function myTag(array, name, role){
 let str0 = array[0]; // My name is
 let str1 = array[1]; // and I am
 
 // we can further manipulate str0, str1, name and role here
 return `${str0}${name}${str1}${role}`;
}
console.log(myTag `My name is ${myName} and I am ${myRole}`); 

/*-------------------------------------------------------------------*/

/*Object.assign()*/

// 1. Object.assign() method can be used to clone an object.
// 2. Object.assign() can also be used to merge objects

let person = { height: '176' };
let clonePerson = Object.assign({}, person);
console.log(clonePerson); // {height:'176'}

/*--------------------------------------------------------------------*/

/*Objec.is()*/
// 1. Object.is() determines if the two values are the same.

let a=10, b=10;
Object.is(a,b); //true
let c=20;
Object.is(a,c);//false
let obj1={a:1};
let obj2={a:1};
let isa=Object.is(obj1, obj2);//false, as both objects don't have same reference
console.log(isa);
let isb= Object.is(obj1, obj1);//true, both objects have the same reference
console.log(isb);



