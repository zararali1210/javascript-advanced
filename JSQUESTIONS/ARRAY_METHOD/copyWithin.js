
/*
The copyWithin() method copies array elements to another position in the array.
The copyWithin() method overwrites the existing values.
The copyWithin() method does not add items to the array.
*/

let str=[1,2,3,4,5,6]
str.copyWithin(0,3,5);
console.log(str);