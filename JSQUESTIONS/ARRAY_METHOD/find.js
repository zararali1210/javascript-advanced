console.log("Welcome to Find Mathod")


/* find()
1. it will not changes the original value
2. it will return undefined if array empty
3. this method function execute for each element of array
4. it will return the first value of element that passed the condition or test
*/

//let array= [1,2,6,5,3,7,8,9];
// let findValue= array.find(value=>{
//                return value >8
// })
// console.log(findValue);

// let findbyFun= array.find(myFunc);
// console.log(findbyFun);

// function myFunc(value){
//     return value>5
// }

// // Orginal value not changed
// console.log(array);


/*
findIndex();
1. from this medhod we can find the index(postion) of the firts element that passes a test
2. it will not changes the orginal value
3. it will return -1 if will not get value from array
4. this method execute each and very element of array
*/
//1. 
let array= [1,2,6,5,3,7,8,9];
let findIndexValue= array.findIndex(value=>{
       return value==9
})
console.log(findIndexValue);
console.log(array); // original value not changed

//2. 
let findIndexByFunction= array.findIndex(myIndexFunction)
console.log(findIndexByFunction);
function myIndexFunction(value){
    return value==3
}

/* indexOf();
1. It will not changes the orginal array
2. we can fina the index value of each and and every element
3. it will execute the method and return first elment  of array
4. If value not match then it will return negative value
*/

let indexArry=[1,2,9,8,4,5,6,3,4];
// let findIndexOf= indexArry.findIndex(value=> value==10)
// console.log(findIndexOf,'--');

/*
 lastIndexOf();
 1. it will not changes the original value
 2. starts at a specified index and searches from right to left.
 3. if value not found or match then it will return -1 value
*/

let lastIndex= indexArry.lastIndexOf(4)
console.log(lastIndex,'lastIndex'); // it will return -1 if not found











