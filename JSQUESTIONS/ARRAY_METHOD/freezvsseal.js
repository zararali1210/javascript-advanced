 // creates an object
 var obj = {
    // assigns 10 to value
    value: 10
};
// creates a non-extensible object
Object.seal(obj);
// the value gets updated to 20

obj.value=20
//https://stackoverflow.com/questions/21402108/difference-between-freeze-and-seal

console.log(obj.value);
console.log(obj);

