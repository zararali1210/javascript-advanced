/*Welcome back in Prototype concept*/

/*
JavaScript is a prototype based language, so, whenever we create a function using JavaScript,
JavaScript engine adds a prototype property inside a function, Prototype property is basically an object 
(also known as Prototype object), where we can attach methods and properties in a prototype object, 
which enables all the other objects to inherit these methods and properties.
*/

const myObject = {
    city: 'Madrid',
    greet() {
      console.log(`Greetings from ${this.city}`);
    }
  }
  
  myObject.greet(); // Greetings from Madrid

  /*List of propertied of prototype*/

  /*
__defineGetter__
__defineSetter__
__lookupGetter__
__lookupSetter__
__proto__
city
constructor
greet
hasOwnProperty
isPrototypeOf
propertyIsEnumerable
toLocaleString
toString
toValueOf  
  */


 const personPrototype = {
    greet() {
      console.log('hello!');
    }
  }
  
  const carl = Object.create(personPrototype);
  carl.greet();  // hello!



