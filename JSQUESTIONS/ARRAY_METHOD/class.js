console.log('Welcome in Class in ES6')

/*
1. Using keyword class to create class
2. always add method named constructor
3. JavaScript Classes are templates for JavaScript Objects.
*/

class Mobiles{
    constructor(name,series){
        this.name=name,
        this.series=series
        this.info=function(price,location){
              return `My phone price is ${price} and buy from ${location}`
        }
    }
    age() {
        let date = new Date();
        return date;
      }
}
let finalResult= new Mobiles('Reliance','Note 3');
console.log(finalResult.name)
console.log(finalResult.age());