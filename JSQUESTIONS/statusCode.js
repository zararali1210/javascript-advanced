/*Welcome Back in Status Code*/

/*
1. 200 OK
2. 201 Created
3. 204 No Content
4. 400 Bad Request
5. 401 Unauthorized
6. 402 Payment Required
7. 403 Forbidden
8. 404 Not Found
9. 408 Request Timeout
10. 500 Internal Server Error
11. 502 Bad Gateway
12. 503 Service Unavailable
13. 504 Gateway Timeout

*/