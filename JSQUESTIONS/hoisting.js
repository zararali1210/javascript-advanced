/*-----------HOISTING-----------------------*/

/*
Hoisting: javascript allocate momory space for all variable and function defined in the the program before execution the code
this is call hoisting
1. Its allow us call the function before even writing them in the code
2. Hoisting is a mechanism where function and varibale are moved to the top of the scope before the execution.
3. 

Let 
We know that variables declared with let keywords are block scoped not function scoped and hence it 
is not any kind of problem when it comes to hoisting.

Note: We have define function in multiple way

1. function x(){
    console.log("First Function");
}

2. var x= function(){
    console.log("Second function")
}

3. var x= function x()=>{
    console.log("Thirtd function");
}

*/

let b = 50;
let a=20;
function codeHoist(){
    a = 10;
    let b = 50;
    console.log(a);
}
codeHoist();
console.log(a);



//10
//console.log(b); // not define because of bloack scope (Inside the function)
// but now it will comde 50

//



function x(){
    console.log("Welcome back in Hoisting " + y);
    var y=10;
}
x();


// next ouput based question

var z=30;
let myFun= function(){
    console.log(z);
    var z=10;// Expected output will be undefined because of hoising in varible case, but in case of function we can call any where.
    
}
console.log(z);
myFun();