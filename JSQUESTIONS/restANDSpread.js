
/*
Note: Both are working with function and array

The rest parameter allows us to pass an indefinite number 
of parameters to a function and access them in an array.
1. Rest operator also can use in destructring
2. The rest operator is used to collect all the remaining values


Spread Operartor: Spread operator allow an iterable to expand in one places, 
where more than zero argument are expected
1.The spread operator is used to create copies of array/objects.

*/

/* Example 1 : With function reduce function  */

// function x(a,b,c,...args){
//     let finalArry= Array.from(args);
//     return finalArry.reduce((acc,current)=>{
//        return acc+current
//     })
// }
// console.log(x(1,2,3,4,5,6,7,8,90))


// /* Example 3 : With map function  */

// function x(a,b,c,...args){
//     console.log()
//      return args.map(data=>{
//          return data+1
//      })
// }
// console.log(x(1,2,3,4,5,6,7,8,90))

/* Example 4 : With map function  */

// function x(a,b){
//     console.log(a+b);
// }
// x(...[1,2])

function sum(x, y, z) {
    console.log(x + y + z);
  }
  
  const numbers = [1, 2, 3];
  
  sum.apply(null, numbers);
  // expected output: 6
  
  //console.log(sum.apply(null, numbers));

  /*==========================EXAMPLE=============================*/

  let a=10;
  {
  let a=20;
  console.log(a);
  }
