/*----------CALLBACKHELL-------------------*/

/*
1. Callback hell is it very complex concept in node js.Each and every callback take an argument which is result
of previous callback
2. Its very complex and messup the function 
3. Its very dificult to understand to not readble the code.
4. Tha code structure look like pyramid , and if there is error in one function it get affected all other function also


HOW TO AVOID

Write comments
Split functions into smaller functions
Using Promises
Using Async/await

*/


/*Example Of Callback Hell*/

// a(function (resultFromA) {
//   b(function (resultFromB) {
//     c(function (resultFromC) {
//       d(function (resultFromD) {
//           console.log(resultFromC)
//       });
//     });
//   });
// });


/*

function a(){
    return new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commitsA"])
        }, 1000);
    })
}


function b(){
    return new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commitsB"])
        }, 1000);
    })
}


function c(){
    return new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commits"])
        }, 1000);
    })
}

a().
then((commitsA)=> a(commitsA)).
then((commitsB)=>console.log("two",commitsB)).
then((commitsC)=>console.log("Commits", commitsC)).catch(err=>{
    console.log(err)
})

// Async- await approach

async function displayCommits() {
    try {
      const user = await a();
      const repos = await b();
      const commits = await c();
      console.log(commits);
    } catch (err) {
      console.log("Error: ", err.message);
    }
  }
  displayCommits();

*/

/*====================EXAMPLE PRACTICE=================================*/


function a(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
           resolve(["RESOLVEDA"])
        },1000)
    })
}

function b(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
           resolve(["RESOLVEDB"])
        },1000)
    })
}
function c(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
           resolve(["RESOLVEDC"])
        },1000)
    })
}
function d(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
           resolve(["RESOLVEDD"])
        },1000)
    })
}

a()
.then((RESOLVEDA)=>b(RESOLVEDA))
.then((RESOLVEDB)=>c(RESOLVEDB))
.then((d)=>console.log("Con", d))
.catch(err=>{
    console.log(err);
})






