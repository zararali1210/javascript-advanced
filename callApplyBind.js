/*
The call() method calls a function with a given this value and arguments provided individually.
you can write a method once and then inherit it in another object, without having to rewrite the method for the new object.

Note: While the syntax of this function is almost identical to that of call(), 
the fundamental difference is that call() accepts an argument list, while apply() accepts a single array of arguments.
1. apply() is call a function with a given this value and provided arguments as well array
2. We are calling directly in call and apply method

1. In case of bind method, it will not execute suddenly and it leave with binding like it will retrun function with this already set
and you can use in latest stage, when we call the function then this already set,
Its invoke later

*/

/*---1st Example---*/

function callMethod(name,city,item){
    this.name=name;
    this.city=city;
    this.item=item;
    this.getInfo=function(val){
        console.log(`This is my ${name} and I'm from ${city} and buy ${item}`)
    }
}

function Toy(name,city, item) {
    callMethod.call(this, name, city, item);
  }


let calling = new Toy("zarar","delhi","watch");
calling.getInfo();


/*----Second Example----*/
let sData = 'Wisen';
function display() {
  console.log('sData value is %s ', this.sData);
}
display.call(); // Cannot read the property of 'sData' of undefined
/*=================================================================================================================*/


/*----------------Apply Mathod----------
Note: While the syntax of this function is almost identical to that of call(), 
the fundamental difference is that call() accepts an argument list, while apply() accepts a single array of arguments.

1. apply() is call a function with a given this value and provided arguments as well array
*/


// const numbers = [5, 6, 2, 3, 7];
// const max = Math.max.apply(this, numbers);
// console.log(max);

const array = ['a', 'b'];
const elements = [0, 1, 2];
array.push.apply(array, elements);
console.info(array); // ["a", "b", 0, 1, 2]




/* Example Apply its accept an arguments*/

/*
function x(item,price,series){

    this.item=item;
    this.price=price;
    this.series=series;
}
function y(item,price,series){
    this.item=item;
    this.price=price;
    this.series=series;
}

const getInfo=function(val){
    console.log(`${val} I just purchase ${this.item} and price is ${this.price} with series is ${this.series}`)
}

let xfinal= new x("Mobiles","9000","W Series");
getInfo.call(xfinal, "Hi");

let yfinal= new y("Mobiles","9000","M Series");
getInfo.call(yfinal, "Hi");
*/

/*Apply: Its accept argument as an array */

/*
function x(item,price,series){

    this.item  = item;
    this.price = price;
    this.series= series;
}
function y(item,price,series){
    this.item  = item;
    this.price = price;
    this.series= series;
}

const getInfo=function(val){
    console.log(`${val} I just purchase ${this.item} and price is ${this.price} with series is ${this.series}`)
}

let xfinal= new x("Mobiles","9000","W Series");
getInfo.apply(xfinal,["Hi"],["Mobiles","9000","W Series"]);

let yfinal= new y("Mobiles","9000","M Series");
getInfo.apply(yfinal,["Hi"],["Mobiles","9000","W Series"]);
*/



/*--------------------Bind Mathod---------------------------------------

1. In case of bind method, it will not execute suddenly and it leave with binding like it will retrun function with this already set
and you can use in latest stage, when we call the function then this already set
 
*/


function x(item,price,series){

    this.item  = item;
    this.price = price;
    this.series= series;
}
function y(item,price,series){
    this.item  = item;
    this.price = price;
    this.series= series;
}

const getInfo=function(val){
    console.log(` ${val} I just purchase ${this.item} and price is ${this.price} with series is ${this.series}`)
}

let xfinal= new x("Mobiles","9000","W Series");
let fn= getInfo.bind(xfinal)
//console.log(fn("A","B","C"))
fn(["Hi"],["Mobiles","9000","W Series"])
// getInfo.apply(xfinal,["Hi"],["Mobiles","9000","W Series"]);
// let yfinal= new y("Mobiles","9000","M Series");
// getInfo.apply(yfinal,["Hi"],["Mobiles","9000","W Series"]);


//https://www.youtube.com/watch?v=Zb4dPi7CANU&ab_channel=Questpond



/*=============================PRACTICE==========================================================================*/


let obj1={
    fname:"Zarar",
    lname:"Ali"
}

let printFullName= function(Param1,Param12){
    console.log(`Hi my name is ${this.fname} ${this.lname} from ${Param1} and ${Param12}` );
}

printFullName.apply(obj1,["Mumbai", "Delhi"]);

let obj2={
    fname:"Rajendra",
    lname:"Desai"
}
let finalBind= printFullName.bind(obj2, "US", "Dubai");
finalBind();


















