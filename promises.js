/*--------------------PROMISE---------------------------*/

/*
1. Promise is javascript object, it used to handle all asynchronous operation
2.  It help in callback hell if we perform multiple operation ,
 the  code will be complex or messy, we will handle promise
3. A promise means which will either be completed or rejected.
4. If promise will give us sucessfully response it prmise completed else rejected.

Nodte: How we can do callback to promise

1.  Promise notify whether the request is fullfilled or rejected
2.  Callback can registered with the .then() for fulfilment or rejection
3. .cath() is use handle the error

We have some method of promise
 1. .all() -> it means it run in parallel, if any one function affected then it will through the error, Its will return in array if all promise are sucess
 2. .race()-> its will return win value (Single value)
 2. .allSettled() -> it will run all and will gave the result with error also if anyonce promise is failed or rejected

What Promise.all does, is just waiting for all the promises to complete.
*/

/*Example of Promise*/

let a= new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commitsA"])
        }, 1000);
    })


let b= new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commitsB"])
        }, 1000);
    })


let c=  new Promise((resolve, reject)=>{
        setTimeout(() => {
            resolve(["commitsC"])
        }, 1000);
    })


Promise.allSettled([a,b,c]).then(res=>{
    console.log(res);
}).catch(err=>{
    console.log(err);
})


