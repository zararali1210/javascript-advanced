
/*

2. Async/Await:

Async/Await is used to work with promises in asynchronous functions. It is basically syntactic sugar for promises. 
It is just a wrapper to restyle code and make promises easier to read and use. It makes asynchronous code look more 
like synchronous/procedural code, which is easier to understand.

await can only be used in async functions. It is used for calling an async function and waits for 
it to resolve or reject. await blocks the execution of the code within the async function in which it is located. 

Error Handling in Async/Await: For a successfully resolved promise, we use try and for rejected promise, 
we use catch. To run a code after the promise has been handled using try or catch, we can .finally() method.
 The code inside .finally() method runs once regardless of the state of the promise.
*/

/*

const doSomethingAsync = () => {
    return new Promise(resolve => {
      setTimeout(() => resolve('I did something C'))
    })
  }
  
  const doSomething = async () => {
    console.log(await doSomethingAsync())
  }
  
  console.log('Before A')
  doSomething()
  console.log('After B')



// Example 2
// https://javascript.plainenglish.io/async-await-javascript-5038668ec6eb
 function x() {
   return new Promise((resolve, reject) => {
     resolve("HI X");
   });
 }
 function y() {
   return new Promise((resolve, reject) => {
     resolve("HI Y");
   });
 }
 async function sequence() {
   return await Promise.all([x(), y()]).then((res) => {
     console.log(res,'----');
   });
 }

 sequence();


 console.log("==========================================")

  /*
Prepending the async keyword to any function means that the function will return a promise.
Even if it's not doing so explicitly, it will internally make it return a promise.
  */
// Example: 1
/*
async function firstAsync() {
  let promise = new Promise((res, rej) => {
    setTimeout(() => res("Now it's done!"), 1000);
  });

  // wait until the promise returns us a value
  let result = await promise;

  // "Now it's done!"
  console.log("BEFORE");
  console.log(result);
  console.log("AFTER");
}

firstAsync();
console.log("Not Done");


//=========================================

// function f1() {
//   console.log('f1');
// }

// function f2() {
//   console.log('f2');
// }

// function main() {
//   console.log('main');
  
//   setTimeout(f1, 1000); 
  
//   new Promise((resolve, reject) =>
//       resolve('I am a promise')
//   ).then(resolve => console.log(resolve))
  
//   f2();
// }

// main();


//===================================





/*--------------------------------*/
console.log("*****************************************************");


// function x(){
//   return new Promise(function(resolve,reject){
//       let err= false;
//       if(!err){
//         resolve("Hi This is resolve State RES")
//       }else{
//         reject("Hi This is reject state REJ")
//       }
//   })
// }
//===================================================
// let p= Promise.resolve(10);

// let x= async()=>{
//    await p.then(res=>{
//           console.log(res);
//     });
// }

// x();


//===========================

/*
function functionA(){
  return "Hi I'm Function"
}
function functionB(val){
  return val;
}

function functionC(val){
  return val;
}
function functionD(val){
  return val;
}



async function asyncTask () {
  try {
    const valueA = await functionA();
    const valueB = await functionB(valueA);
    const valueC = await functionC(valueB);
    let a= await functionD(valueC);
    console.log(a);
  } catch (err) {
    logger.error(err);
  }
}

asyncTask();
*/

let name = async() =>{
  let output = await Promise.resolve("GeeksforGeeks");
  return output;
}
name().then(res=>{
  console.log(res)
})
//console.log(name());

function p(){
return new Promise(function(resolve,reject){
          resolve("Hi, Its resolve")
})
}

p().then(res=>{
  console.log(res);
})


for(var i=5; i> 0; i--){
  console.log(i);
}














