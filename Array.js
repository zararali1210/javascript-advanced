// /*-Array Concepts-*/

// // 1St Method

// const fist=["Manish", "Chandan", "Piyush", "Manish", "Sunil", "Chandan"];
// const fisrtget= [... new Set(fist)]

// console.log('First Example', fisrtget);


// //2nd Method

// const secongGet= Array.from(new Set(fist))

// console.log('Second Example', secongGet);

// //3rd Method

// const thirdMethod=[1,2,3,4,5,6,1,3]
// const thirdFinalValue= {};
// for(i=0;i<thirdMethod.length;i++){
//     let value= thirdMethod[i];
//     thirdFinalValue[value]='';
// }
// const valthird= Object.keys(thirdFinalValue);
// console.log('Third Example',valthird);


// //4Th Mehtod

// const ages = [26, 27, 26, 26, 28, 28, 29, 29, 30]
// const getUniqueData= (value,index,self)=>{
//     return self.indexOf(value)===index
// }

// const finalUn= ages.filter(getUniqueData);
// console.log('4rth Example',finalUn);


// //5th Mthod:

// const filter = [26, 27, 26, 26, 28, 28, 29, 29, 30]
// const uniquefilter = filter.filter((value, index, self) => self.indexOf(value) == index)
// console.log('5th Example',uniquefilter)


// var show = (a, b=200) => {  
//     console.log(a + " " + b);  
// }  
// show(100,400);



// const ArrayOf= Array.from("zarar");
// console.log(ArrayOf);


// Map();

let commonArray=[1,2,3,4,5,2,3];
let finalMap= commonArray.map(val=>val*2);
console.log("finalMap",finalMap);

//=====================================================
// Filter();
function chkEven(value){
  return value%2==0
}

let filterArray= commonArray.filter(chkEven);
console.log(filterArray);


//===========================

//Sort()
let sortArray= commonArray.sort((a,b)=>a>b?-1:1)
console.log(sortArray);

//=============================

let joinArray= [1,2,3,4,5,6];
let finalJoin= joinArray.join('');
console.log(finalJoin);


//Reudec

// let sum=0;
// let arraSum=[1,2,3,4,5];

// for(let i=0;i<arraSum.length;i++){
//    sum= sum+ arraSum[i];
//    return sum;

// }

// console.log(arraSum);


//Shallow Copy==============================

let shallowArray=[1,2,3,4,5,6,{"Name":"Zarar"}];
let anotherArray=["ALI"];
let finalArrayS= anotherArray.concat(shallowArray)
finalArrayS[0]='Changes Name';
console.log(finalArrayS,shallowArray);


let fromArray= [1,2,3,4,5,6];
let finalFromArray = Array.from(fromArray);
finalFromArray[0]="from";
console.log('fromArray',fromArray);
console.log(finalFromArray);



//==============

const a = 5
let   b = a;
console.log(b);



let obj1= {
    firstName: 'John',
    lastName: 'Doe',
    address: {
        street: 'Amphitheatre Parkway',
        city: 'Mountain View',
        state: 'CA',
        country: 'USA'
    }
}

let finalObj= Object.assign({},obj1)
finalObj.firstName="Zarar";

//console.log(obj1)

let athletes = new Array(3);
athletes[0]='0'
console.log(athletes)



//=============

let mountains = ['Everest', 'Fuji', 'Nanga Parbat'];
//let test= mountains;
let anotherArrayM=["k2"];

let finalMou= Array.from(mountains)
finalMou[0]="Tes";

console.log(mountains,finalMou);


//================================================
// Deep coping

let deepArray=[1,2,3,4,{"Name":"Zarar"}]
let finalDeep= JSON.parse(JSON.stringify(deepArray));
finalDeep[4]["Name"]="Changed";
finalDeep[0]=7;
console.log(deepArray);
console.log(finalDeep);

// Shallow Coping

// concat();
let sCopy=[1,2,3,4,5,7,89,0]
let anotherA=["Test"]
let finalSc= anotherA.concat(sCopy);
console.log(sCopy);
console.log(finalSc);

// Spread()

let finalSpread=[...sCopy]
finalSpread[0]="Naam";

console.log(sCopy)
console.log(finalSpread);
//========================================================================

let object1={
    fname:"Zarar",
    lname:"ALI",
    
}

const greets=function(){
    console.log(this.fname + " " + this.lname)
}

let object2={
    fname:"Mukhtar",
    lname:"ALI",
}


greets.call(object2);
greets.call(object1);

const array = ["a", "b", "c"];
const arrayEntries = array.entries();

for (let element of arrayEntries) {
  console.log(element);
}




























