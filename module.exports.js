/*---------------Module Export---------------------------/

/*
module.exports is special object which included in JS file in Node JS application by default.
1. module is a varible it represent the current module.
2. export is an object that will be exposed as module

Example:->

in data.js

modile.exports={
    fname:"Zarar Ali"
    lname: "Ali"
}

in app.js

let data= required('./data')
console.log(data.fname);


Another Example:->

module.exports = function (firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.fullName = function () { 
        return this.firstName + ' ' + this.lastName;
    }
}

var person = require('./Person.js');
var person1 = new person('James', 'Bond');
console.log(person1.fullName());

*/