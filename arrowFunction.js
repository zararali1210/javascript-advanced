/*

1. Normal function:
-  We have write the code like normal function we defined
- We have return the value
- in Normal function , not required pass argument in the function, but result can print
- this keyword, it pointing to current object(Parent object) of instance



2. Arrow Function
- We have define a function in single line with arrow
- Not required return keyword in single line code
- In arrow function it will thrown error
- this keyword, it pointing to window object 

*/

/*=============================EXAMPLE PRACTICE===========================================*/

function x(){
    console.log("Welcome in Normal Function");
}
x();

let y=()=>{
    console.log("Welcome to Arrow Function");
}
y();


// newt one with return keyword


function ab(){
    return "Welcome with return keyword"
}
console.log(ab());


let abc=()=>"Welcome with arrow function without return keyword";

console.log(abc());


// In pointing to parent or window object


let obj={
    fname:"Zarar",
    lname:"Ali",
    obj1:function(){
        console.log("It reference to the parent object" + this.fname);
    },
    obj2:function(){
        console.log("It reference to the window object" + this.fname);
    }
}

obj.obj1();
obj.obj2();


















