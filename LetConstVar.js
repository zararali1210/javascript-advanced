/**************LET COST AND VAR ******************* */

/*
var: if we declare from var, then we can also declare it again with same name
and we can re-assign the value in var also. it a global scope and it run in global object
let: If we declare the varible with let then again we can not declare it but we can assign the value
const: if we declare the variable then we can't declare the agin, event we can't assign the value
its also a block scope.

*/

/*-------Example VAR--------*/

var a= "I'm developer";
var a= "I'm not developer";
console.log(a);

/*-------Example let --------*/

//let b= "I'm let variable"
//let b= "I'm not let varible";
//console.log(b); // error will return: b already exist

// 2nd Example 

let b= "I'm let variable"
b= "I'm not let variable";
console.log(b); // error will not return error: it means we can again define the let varible but we can changes the value

/*-------Example const --------*/

//const c= "I'm const variable"
//const c= "I'm not const varible";
//console.log(c); // it will retrun error also, c already defined

//const c= "I'm const variable"
 //c= "I'm not const variable";
//console.log(c); // it will retrun error also, "TypeError: Assignment to constant variable." it means we can't define again

/*--------------Example--------*/

// {
//     let leta=10;
//     const constb=20;
//     var varc= 30;
//     console.log(leta);
//     console.log(constb);
//     console.log(varc);
// }
//     console.log(leta);
//     console.log(constb);
//     console.log(varc);


 /* ---------------Shadowing Concept--------------------*/   
let o=100;
 {
  let o=10;
  const p=20;
  var q=30
 }

console.log(o);








