/*----ARRAY-----------concat---------No Change in Original value*/

let concat1 = [[23]];
let concat2 = [89, [67]];
let finalConCat= concat1.concat(concat2)
console.log(finalConCat);

/*-------------copywithin------------------ Change in Original value
formula:  copywithin(start, default 0, end array.length)
*/
let copyArray=[1,2,3,4,5,6];
//copyArray.copyWithin(0,3)
//copyArray.copyWithin(2,3)
//copyArray.copyWithin(3,0,5)
//console.log(copyArray);

/*-------------entries------------------ No Change in Original value */
let languages = ["HTML", "CSS","JavaScript", "ReactJS"];
let g = languages.entries();
console.log(g.next().value)
console.log(g.next().value)
console.log(languages)
// for(let val of g){
//     console.log(val);
// }

/*-------------every------------------ No Change in Original value */

let everyArray=[1,2,3,4,5,6,11]
let finalEvery= everyArray.every(val=>val<10)
console.log(finalEvery) // it will retrun true or false

/*-------------fill------------------ Change in Original value */

//Rule: (start, default 0, end array.length)
var arrayFill = [1, 23, 46, 58];
//arrayFill.fill(87,2,4);
arrayFill.fill(87,0,4);
console.log(arrayFill)

/*-------------find------------------ No Change in Original value */

let findValue=[1,2,3,4,5,6,11]
let finalFind= findValue.find(value=>value>12);
console.log(finalFind);

/*-------------findIndex------------------ No Change in Original value */

let findIndexValue=[1,2,3,4,5,6,10,12]
let finalIndex= findIndexValue.findIndex(value=>value==11);
console.log(finalIndex);


/*-------------flat()------------------ No Change in Original value 
Itd change only nested depth element
*/

let flatArray= [[1, 2], [4, 8],[[[[[[[["9"],[1]]]]]]]]];
let finalflat= flatArray.flat(Infinity);
console.log(finalflat);

/*-------------flatMap------------------ No Change in Original value 
Flat map, it will with depth one with split into a single value in array
*/

let flatMap= ["My name","is", "Zarar Ali"]

let flatFinal= flatMap.flatMap(value=>{
    return value.split(" ")
});
console.log(flatFinal);


/*-------------forEach()------------------ Change in Original value */

let eachArray=[1,2,3,4,5,6,10,12]
  eachArray.forEach((value, index)=>{
     return eachArray[index]=value+1;
 });
console.log(eachArray);

/*-------------Array.from()------------------ Change in Original value */
//Array.from(arrayLike, mapFn, thisArg)
// its create an array from string, map, set
//let from=new Set(['foo', 'bar', 'baz', 'foo']);
var myArr = Array.from("Zarar");
console.log(myArr);


/*-------------forEach(includes)------------------ No Change in Original value */

let isIncludeArray=[1,2,3,4,5,6,10,12]
let isValue= isIncludeArray.includes(12);
console.log(isValue);


/*-------------indexOf()------------------ No Change in Original value */

let indexOfArray=[1,2,3,4,5,6,10,12]
let finalIndexValue= indexOfArray.indexOf(12);
console.log(finalIndexValue);

/*-------------isArray()------------------ No Change in Original value */

let isArrayValue={}
let isArray= Array.isArray(isArrayValue);
console.log(isArray);

/*-------------join()------------------ No Change in Original value */
var a = [1, 2, 3, 4, 5, 6];
console.log(a.join('|'));
console.log(a);


/*-------------key()------------------ No Change in Original value */
var keyArray  = ["Zarra","Ali"];
let keyValue  = keyArray.keys();
console.log(keyArray);
for(let key of keyValue){
    console.log(key);
}

/*-------------lastIndexOf()------------------ No Change in Original value */
//array.lastIndexOf(element, start)
let lastIndexArray=[1,2,3,2,2,12,5,6,10,12,1]
let lastIndexValue= lastIndexArray.lastIndexOf(10);
console.log(lastIndexValue,'--');


/*-------------map()------------------ No Change in Original value */
var mapArray = [1, 2, 3, 4, 5, 6];
let mapValue= mapArray.map(value=>value*2)
console.log(mapValue);
console.log(mapArray);


/*-------------Array.of()------------------ No Change in Original value */
// its simply return the array
let finalOfArray= Array.of(1, 2, 3, 4, 5, 6)
console.log(finalOfArray);


/*-------------pop()------------------ Change in Original value */
var popArray = [1, 2, 3, 4, 5, 6];
popArray.pop();
console.log(popArray);

/*-------------push()------------------ Change in Original value */
var pushArray = [1, 2, 3, 4, 5, 6];
pushArray.push(1);
console.log(pushArray);

/*-------------reduce()------------------ No Change in Original value */
var reduceArray = [1, 2, 3, 4, 5, 6];
let finalReduce= reduceArray.reduce((acc,current)=>{
                   return acc+current
})
console.log(finalReduce);

/*-------------shift()------------------ Change in Original value */

var shiftArray = [1, 2, 3, 4, 5, 6];
shiftArray.shift();
console.log(shiftArray);

/*-------------unshift()------------------ Change in Original value */

var shiftArray = [1, 2, 3, 4, 5, 6];
shiftArray.unshift();
console.log(shiftArray);

/*-------------slice()------------------ No Change in Original value */

let sliceArray = [1, 2, 3, 4, 5, 6];
let getSlice =sliceArray.slice(5);
console.log(getSlice);
console.log(sliceArray);

/*-------------splice()------------------ No Change in Original value */

//Array.splice( index, remove_count, item_list )
let spliceArray = [1, 2, 3, 4, 5, 6];
let getSplice =spliceArray.splice(3,2);
console.log(getSplice);
console.log(spliceArray);

/*-------------toString()------------------ Change in Original value */

var stringArray = [1, 2, 3, 4, 5, 6];
stringArray.toString();
console.log(stringArray,'-');


/*-------------values()------------------ No Change in Original value */

var iteratorArray = [7, 2, 3, 4, 5, 6];
var iterator      = iteratorArray.values();
console.log(iterator);
for(let key of iterator){
    console.log(key);
}

/*-------------toLocaleString()------------------ No Change in Original value */
console.log(iteratorArray.toLocaleString());

console.log(iteratorArray);


// const weakmap = new WeakMap();
 
// const key = {};
// weakmap.set(key, 6);
// console.log(weakmap.get(key));
// console.log(weakmap.delete(key));

// const weakmap = new WeakMap(); 
// const key = {}; 
// weakmap.set(key, 'gfg');
// console.log(weakmap.has(key));
 

// const weakmap1 = new WeakMap();
  
//     const key1 = {};
//     const key2 = {};
//     const key3 = {};
  
//     weakmap1.set(key1, 'G');
//     weakmap1.set(key2, 'F');
//     weakmap1.set(key3, 'G');
//     console.log(weakmap1.get(key1));




    // const weakset = new WeakSet(); 
    // const object1 = {}; 
    // const object2={};
    // weakset.add(object1); 
    // console.log(weakset.delete(object1)); 
    // console.log(weakset.has(object1));  
 //========================================================================= 
//https://javascript.info/map-set


// let x= [1,2,3,4,5,9,2];

// function isPositive(val){
//     return val>0
// }
// let z= x.every(isPositive)
// console.log(z);



function callMethod1(name,city,item){
    this.name=name;
    this.city=city;
    this.item=item;
}


function callMethod2(name,city,item){
    this.name=name;
    this.city=city;
    this.item=item;
}

var getInfo=function(){
    console.log(`This is my ${this.name} and I'm from ${this.city} and buy ${this.item}`)
}

//let calling1 = new callMethod1("Nadeem","delhi","Ring");
let calling2 = new callMethod2("Nadeem","delhi","Ring");
//getInfo.call(calling1,"Nadeem","delhi","Ring");
let fn= getInfo.bind(calling2);
fn(["Nadeem","delhi","Ring"]);


//================isFrozen==================

object = {
        property: 'hi geeksforgeeks'
        };
        Object.freeze(object);
        console.log(Object.isFrozen(object));

//==================hasOwnProperty==&& preventExtensions==========================

const geeks1 = {"prop1": 555}; 
console.log(Object.preventExtensions(geeks1),"----"); 
delete geeks1.prop1; 
console.log ( geeks1.hasOwnProperty ( "prop1" ) );

//======================endsWith=====================================

var str = 'Geeks for Geeks';
var value = str.endsWith('for',8);  
console.log(value);

var A = 'Geeks for Geeks';
 
    b = A.slice(0,5);
    c = A.slice(6,9);
    d = A.slice(10);


    console.log(c);


 //=====================Sync Calling  and Async===========================  
 
 

 function handler(e) {
    console.log("handler triggered");
    doSomething();
    console.log("handler done");
  }
  
  function doSomething() {
    doThis();
    setTimeout(function(){
        doThat();
    })
   
    doTheOther();
  }
  function doThat() {
    console.log("doThat - start & end");
  }
  
  function doThis() {
    console.log("doThis - start & end");
  }
  function doTheOther() {
    console.log("doTheOther - start & end");
  }
  doSomething();
  //=================================================


  function f1() {
    console.log('f1');
   }
   
   function f2() { 
       console.log('f2');
   }

   function f3() { 
    console.log('f3');
}
function f4() { 
    console.log('f4');
}

// asec=1000; 
// setTimeout(f1,asec*1);
// setTimeout(f2,asec*2);
// setTimeout(f3,asec*4);

// setTimeout(function() {
//   console.log("Outside");
// setTimeout(function() {
//        console.log("Inside");
//     }, 2000);
// }, 2000);


//=========================================================

// function p(){
//     return new Promise((resolve, reject)=>{
//         let x=1;
//         if(x==10){
//         resolve('OK')
//         }else{
//         reject('Not OK')
//         }
//     })
// }

// console.log(p());


function unex(){

    process.on('uncaughtException', function(err) {
  
        // Handle the error safely
        console.log(err,)
    })
    // try {
      
    //     // The synchronous code that
    //     // we want to catch thrown
    //     // errors on
    //     var err = new Error('Hello')
    //     throw err
    // } catch (err) {
          
    //     // Handle the error safely
    //     console.log(err)
    // }
    process.on('uncaughtException', function (err) {
        console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
        console.error(err.stack);
        // Send the error log to your email
        sendMail(err);
        process.exit(1);
      })
}

unex();










// readFile and createReadStream















































