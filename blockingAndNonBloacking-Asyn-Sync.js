/*------------------Blocking or Synchronous-----------------------*/
/*
Node js run on virtual environment and its a single threaded, Node js based on event driven non blocking i/o model
1.  In this way, code run synchronously. its means code will run line by line.
2. It will wait until get the response,  the executation of code wil block

Example 1: Blocking (Waiting request until get response then it will move into next line)

var fs = require("fs");
var data = fs.readFileSync('input.txt'); 
console.log(data.toString());
console.log("Program Ended");

Note: Its a Blocking request. it will wait until get response of data and will not move into next line


/*------------------Non Blocking or ASynchronous-----------------------*/

/*

1. Here, the executation of code run imidiately. it will not wait until get the response
2. It will not run the code line by line. So node js can handle large number of request and no wait for response
it will jump into next line or it run the code ASynchronous way.

Example 2: Non Blocking (Below one is call back function)

fs.readFile(data, function(err,data){
    if(err){
        return error.console(err);
    }else{
        return data.toString
    }
})

================================

1. Asynchronous

The architecture of asynchronous explains that the message sent will not give the reply on immediate basis just like we send the mail but do not get the reply on an immediate basis. It does not have any dependency or order. Hence improving the system efficiency and performance. The server stores the information and when the action is done it will be notified.

2. Non-Blocking

Nonblocking immediately responses with whatever data available. Moreover, it does not block any execution and keeps on running as per the requests. If an answer could not be retrieved then in those cases API returns immediately with an error. Nonblocking is mostly used with I/O(input/output). Node.js is itself based on nonblocking I/O model. There are few ways of communication that a nonblocking I/O has completed. The callback function is to be called when the operation is completed. Nonblocking call uses the help of javascript which provides a callback function.

Asynchronous VS Non-Blocking
Asynchronous does not respond immediately, While Nonblocking responds immediately if the data is available and if not that simply returns an error.
Asynchronous improves the efficiency by doing the task fast as the response might come later, meanwhile, can do complete other tasks. Nonblocking does not block any execution and if the data is available it retrieves the information quickly.
Asynchronous is the opposite of synchronous while nonblocking I/O is the opposite of blocking. They both are fairly similar but they are also different as asynchronous is used with a broader range of operations while nonblocking is mostly used with I/O.


*/
