/*------------Child Process-----------------------*/

//The child processes communicate with each other using a built-in messaging system.
/*
Child Process: Node JS process in a non blocking way, meaning that is single threaded can manage the multiple request
at same time.
1. But sometime we have to do some heavy calculation on the server side, since node js single threaded, we can not block main  thread 
for these kind of calulation
2. So we need to use inbuild library call child_process
3. child_process open a process by creating pipe

We have 4 way to intiate the child process
1. exec()
2. execFile();
3. spwan();
4. fork();

spwan() and fork() both are way to create the child process in node js in order to hnadle increasing workload

Differences between Spawn and Fork
While both sound very similar in the way they transfer data, there are some differences.

1. Spawn is useful when you want to make a continuous data transfer in binary/encoding format — e.g. transferring a 
Gigabyte video, image, or log file.
2. When spawn is called, it creates a streaming interface between the parent and child process. 
Streaming Interface — one-time buffering of data in a binary format.
3. Its a initiates a command new process, we can pass a command as a argument to it.
4. It is used when we want the child process to return a large amount of data back to the parent process.

==== Fork();
1. Fork is useful when you want to send individual messages — e.g. JSON or XML data messages
2. When fork is called, it creates a communication channel between the parent and child process Communication Channel — messaging
3. It is used to separate computation-intensive tasks from the main event loop.
4. it is used to create a new instance of the V8 engine to run multiple workers to execute the code.
*/

// let cp= require('child_process')
// //cp.exec('start www.google.com') // in this we can open or execute the file

// let output= cp.exec('node input.txt');
// console.log(output.toString())

let { execFile }=require('child_process');

//command shoud be like start www.google.com and dir
// exec('start www.google.com',(err,stdout,stderr)=>{
//     if(err){
//         console.log(`This is err- ${err.message}`); // for syntex error
//     }
//     if(stderr){
//         console.log(`This is stderr ${stderr}`); // if missing any argument or wrong command
//     }
//     if(stdout){
//         console.log(`This is stdout ${stdout}`); // for syntex error
//     }

// })

// let { spawn }=require('child_process');
// let child  =  spawn('dir');

// child.stdout.on('data',(data)=>{
//     console.log(data)
// })

let fs= require('fs');

fs.stat('input.txt', (err,data)=>{
   console.log(data);
})







