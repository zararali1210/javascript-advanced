
// The synchronous code
//Approach 1: Using try-catch block:

try {
	
	// The synchronous code that
	// we want to catch thrown
	// errors on
	//var err = new Error('Hello')
	//throw err
} catch (err) {
	
	// Handle the error safely
	console.log(err)
}


// Approach 2: Using Process:

/*
 A good practice says, you should use Process to handle exception. A process is a global object 
 that provides information about the current Node.js process. The process is a listener function that 
 is always listening to the events.
Few events are:

Disconnect
Exit
Message
Multiple Resolves
Unhandled Exception
Rejection Handled
Uncaught Exception
Warning

process.on('uncaughtException', function(err) {
    // Handle the error safely
    console.log(err)
})
*/

var domain = require('domain');
var d = domain.create();
var fs = require('fs');

d.on('error', function(err) {
  console.error(err);
});

d.run(function() {
  fs.readFile('somefile.txt', function (err, data) {
    if (err) throw err;
    console.log(data,'==========');
  });
});



