
/* Shallow Copy: It work for top level element. It means some value of variable are connected from 
original variable and shallow copy is faster the deep coopy. When we do copy object from original object and changes into object
it will refelect in both original and copied object.
Its stored in same memory location. 
For Copy we have some method
- slice()
- Spread Operator
- Array.from
- concat();

- Deep Copy: Its work on nested level element. its slower then shallow copy. Its stored in 2 different memory location.
- If we changes in copied object it will not refect in original object.
- Tha value of variable are discinnected from original object
- We can just use: JSON.parse(JSON.stringify("Object Name"))
*/



/*----Deep Copy Example 1----*/

let commonObject= {
    eid: "E102",
    ename: "Jack",
    eaddress: "New York",
    salary: 50000,
    nestedObject:{
        name: 'Manjula',
         age: 25, 
         Profession: 'Software Engineer'
    }
}

// let anotherDeep= commonObject
// anotherDeep.eid="E103";
// console.log("anotherDeep", anotherDeep);
// console.log("============================");
// console.log("commonDeep",commonObject); 
// Note: So here its refelected in both object, we will use JSON.parse(JSON.stringify("OBJECT"))

let finalDeep= JSON.parse(JSON.stringify(commonObject))
finalDeep.eid= "E103";
finalDeep.nestedObject.name="Zarar";

console.log("anotherDeep", finalDeep);
console.log("============================");
console.log("commonDeep",commonObject); 

// Note: Now here is its stored in two different memory location, copied object is disconnected frim original object




/*--------------------------------SHALLOW COPY---------------------------------*/

let a= [1,2,3,4,5,6,7,8];
let b=a;
b[0]=0;
console.log(b);
console.log("=================");// Note: Here also changes ib both same, a and b. because its pointing by reference same memory location
console.log(a);

/* Note: We have some method to copy from  original array
1. concat()
2. slice()
3. spread();
4. Array.from();

*/

 /* 1. concat() Example */

 let x = [1,2,3,4,5,6,7,8,9]
 let y = [0];
 let finalConcat= y.concat(x);
 console.log(finalConcat);
 console.log("==========");
 console.log(x);

/* 2. spread() Example */

let finalSpread= [...x]
finalSpread[0]=10;
console.log(finalSpread);
console.log("==============");
console.log(x);


/* 3. slice() Example */

let finalSlice= x.slice(0);
finalSlice[0]=11
console.log(finalSlice);
console.log("==============");
console.log(x);


/* 4. Array.from() Example */


let finalForm= Array.from(x);
finalForm[0]=99;
console.log(finalForm);
console.log("==============");
console.log(x);




















































































