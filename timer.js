/*------------------------------SetTimeOut()-----------------------*/

/*
setTimeOut(): When you want to run some function asynchronously, after specified the delay and after finishing the current scope
1. setTimeOut callback will be registered in the time queue

setImmediate(): When you want run some function asynchronously, but as soon as posible and after finishing the current block.
1. seImmediate callback will be registered in the check queue

process.nextTick(): It will execute the code before initialze the event loop, it will process after the execution of current phase
2. nextTick callback will be registered in the nextTickQueue
*/

/*
Note:

1. setTimeout and the callback will not be added to the call stack cumulative. 
It is guaranteed that the call stack will be emptied first before the callback will be pushed unto it.
2. It is. And it is popped from the call stack as soon as the callback is registered, so this hardly takes any time.

3. When the given delay has passed, the callback will be queued. Then when the call stack is empty, JS will process the event queue, 
and the callback will be called. Also this callback will be added to the call stack.

*/

/*----Example---*/

function add(a,b){
    console.log(a+b);
}
//add(2,3)

setTimeout(() => {
    console.log("this is setImeout");
    add(2,3);
});

setImmediate(()=>{
    console.log("this is setImmediate");
    add(3,4)
})

process.nextTick(()=>{
    add(4,5)
    console.log("this is nextTick");
})

// Here sequence are: nextTick, setTimeout, setImmediate



const p1 = Promise.resolve(3);
const p2 = 1337;
function p3()
{
new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("foo");
  }, 100);
});
}

Promise.all([p1, p2, p3]).then(values => {
  console.log(values); // [3, 1337, "foo"]
});
//===================================================

// function x(){
// return new Promise((resolve, reject) => {
//     setTimeout(() => 
//     resolve('p1_delayed_resolution'), 
//     1000);
//   });
// }
// console.log(x(),'----------------');4


