// Explain the concept of URL module.
// The URL module of Node.js provides various utilities for URL resolution and parsing. 
//It is a built-in module that helps in splitting up the web address into a readable format:

// 1
// var url = require('url');
// var url = require('url');
// var adrs = 'http://localhost:8082/default.htm?year=2019&amp;amp;month=april';
// var q = url.parse(adr, true);
// console.log(q.host); //returns 'localhost:8082'
// console.log(q.pathname); //returns '/default.htm'
// console.log(q.search); //returns '?year=2019 and month=april'
// var qdata = q.query; //returns an object: { year: 2019, month: 'april' }
// console.log(qdata.month); //returns 'april'

var url  = require('url');
var adrs = 'http://localhost:8082/default.htm?year=2019&amp;amp;month=april';
var q    = url.parse(adrs,true);
console.log(q.query);