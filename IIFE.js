/*
1. An Immediately-invoked Function Expression (IIFE for friends) is a way to 
execute functions immediately, as soon as they are created.
2. An IIFE is a good way to secure the scope. You can use IIFEs to prevent global variables' 
definition issues, alias variables, protect private data, 
and avoid conflicts of using many libraries that export the same object name
*/

// (function xyz(num){
//   console.log(num*num);
// })(6)

(function(x){
    return (function(y){
         console.log(x);
    })(6)
})(4)

// Note: In above; it will serach in chil scope of not find then it will go in parent scope so it will print 4


  