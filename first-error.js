/*
Error-First Callback: Is a function and its a  callback function which is first argument is reserved for err if error comes then it will return err
and second parameter is data , in data will get response 

*/

let fs = require('fs');
// fs.readFile('input.txt','utf-8',(err,data)=>{
//     if(err){
//       throw err
//     }else{
//         console.log(data);
//     }
// })

// fs.open('fs.txt', 'a', function(err, fd) {
//     if (err) {
//        return console.error(err);
//     }
//     console.log("File opened successfully!");     
//  });
//https://www.tutorialspoint.com/nodejs/nodejs_file_system.htm