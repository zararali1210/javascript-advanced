/**CLUSTER**/
/*
Cluster is bacically use for performance and more scability,
node is run on single threaded,in it we have once CPU
and one thread so might possible to distribute the laod

1. The cluster is a collection of child Process(Worker)  of parent process
2. Using the fork() method of node child_process, worker as created child process of parent process
3. A cluster has several event, the most common are online event, to  indication the activation worker and 
exist to indicate the end of the worker

4 Phase:

1.  Install module cluster and os npm
2.  Check cluster with master , master is main cluster
3. 
*/

var cluster = require("cluster");
var numCPUs = 3;

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
} else {
  console.log(process.pid);
}
cluster.on("online", function (worker) {
  console.log("Worker " + worker.process.pid + " is online");
});

cluster.on("exit", function (worker, code, signal) {
  console.log(
    "Worker " +
      worker.process.pid +
      " died with code: " +
      code +
      ", and signal: " +
      signal
  );
  console.log("Starting a new worker");
  cluster.fork();
});
    