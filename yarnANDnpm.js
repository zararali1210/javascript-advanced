/*-------------YARN and NPM------------------*/
/*
NPM. is node package manager it is by default manager and maintained by NPM
1.Use npm install to install new dependencies ,
or to update existing dependencies (e.g. going from version 1 to version 2).
2. NPM is a package manager, which help you install libraries, plugins, frameworks and applications.
3. Npm is a tool that use to install packages. Npx is a tool that use to execute packages.


Note: For NPM Points (Its developed by NPM inc)
1. npm install
2. npm install package_name
3. npm uninstall package_name
4. npm install package_name -save-dev
6. npm update package_name
7. npm install -g package_name

Note: For Yarn Points (Its developed by Facebook)
1. yarn
2. yarn add package_name
3. yarn remove package_name
4. yarn add package_name -dev
6. yarn upgrade package_name
7. yarn global add package_name
*/


/*
NPX: The npx stands for Node Package Execute and it comes with the npm, 
when you installed npm above 5.2.0 version then automatically npx will installed. It is an npm package runner 
that can execute any package that you want from the npm registry without even installing that package.
*/


/*
Better:

Speed and Performance. As mentioned above, while NPM installs dependency packages sequentially,
Yarn installs in-parallel. Because of this, Yarn performs faster than NPM when installing larger files

*/


