console.log("Welcome in JS Home Page");


// What is this keyword ?
/*
1. this keyword is a global object, it will return object when you console this keyword in browser.
2. A this keyword in normal function will point to parent object
3. And if we are using this key in arrow function then it will point to window object
*/

//var firstName="Zarar From Delhi"
var student = {
  firstName: "Ram Charan",
  class: 10,
  // getValue:function(){
  //   var firstName="New Test"
  //   console.log(this.firstName);
  // }
getValue:()=>{
    console.log(this.firstName,);
  }
};

student.getValue();


// Output based QA, How we can access name here

function x(){
    return {
        name:"Zarra",
        ref(){
         return this.name;
        }
       
    }
}
var y= x();
console.log(y.ref());

// Output based QA: It was call back so we need to wrap inot function 

let obj={
    name: "Raj",
    getData(){
        console.log(this.name);
    }
}
setTimeout(function(){
    obj.getData()
}, 1000);



/*==============================EXAMPLE PRACTICE===============================================*/

var fname="Ramesh";
var lname="Kumar";
var myObj = {
  fname:"zarar",
  lname:"ali",
  getValue:()=>{
    console.log("This is my full name: " + this.fname +" " + this.lname);
  }
  // getValue:function(){
  //    console.log("This is my full name: " + this.fname +" " +this.lname);
  // }
}
/* 
Note: A normal function, this keyword pointing to the local object,
and with arrow function it pointing to window object
*/

myObj.getValue();


// next one

let finalObj={
  age:24,
  city:"delhi",
  village:"salai",
  getFun:function(){
     var age=30;
     console.log(this);
  }
}

finalObj.getFun();// o/p: 24(this keyword pointing to the parent object)

// next one

function makeUser(){
  return {
    name :"Zarar with ref",
    ref(){
       return this.name;
    }
  }
}

let user= makeUser();
console.log(user.name);


// next one Out put based question 

let calc = {
  total: 0,
  add(a) {
    this.total += a;
    return this;
  },
  mul(a) {
    this.total *= a;
    return this;
  },
  substract(a) {
    this.total -= a;
    return this;
  },
};

let resul = calc.add(2).mul(8).substract(2).add(10);
console.log(resul.total);

