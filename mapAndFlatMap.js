/*---------------TOPIC------------------*/

//1 Flat array : Flattening an array is a process of reducing the dimensionality of an array
// 2. it will work with nested depth

const arrayList = [[1, 2], [4, 8],[[[[[[[["9"],[1]]]]]]]]];
let finalArray= arrayList.flat(Infinity).reduce((a,b)=>a+b)
console.log(finalArray); // final result in single value
console.log(arrayList) // it will not the orginal value

// 2 Flat map, it will with depth one with split into a single value in array

// let flatMap= ["My name","is", "Zarar Ali",["Hi",["ALI"]]]

// let flatFinal= flatMap.flatMap(value=>{
//     return value.split(" ")
// });
// console.log(flatFinal);

const arr1 = ["it's Sunny in", "", "California"];
let a= arr1.flatMap(x => x.split(" "));
console.log(a)
//console.log(arr1);