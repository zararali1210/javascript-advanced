/*

A temporal dead zone (TDZ) is the area of a block where a 
variable is inaccessible until the moment the computer completely initializes it with a value
*/