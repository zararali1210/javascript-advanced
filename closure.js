/*
CLOSURE(): 
1. Closure is a concept in javascript, its comes under ECMA6
2. Closure function along with Lexical Environment bundle together form as closure. This is called closure.
3. Lexical Environment is data structure hold the identifier-varibale maping
4. So in other words, we have 2 functions, like child function and parent function , so in that case we can access
outer function variable  from child function.
5. Closure are function inside function and it make normal function statefull
6. Closure are reference variable in the outer scope from it inner scope

Where we can use:
1. settimeOut()
2. Currying()
3. Module Design Pattern 
4. function 

Note: let and const  are stored in reserved memory space , unlike var.
var is use global window object, so we can access anywhere.

*/

/*----Example 1----*/

function  x(){
  let a=10
    function y(){
        let a=20;
        console.log(a); // o/p:20
    }
    return y();
}
x();

/*----Example 2----*/

function  a(){
  let ab=10
    function b(){
        let ab=20;
        function c(){
          let ab=40;
          console.log(ab); // o/p: 40  console will print nearest value
      }
      return c()
    }
    return b();
}
a();

/*----Example 3----*/

for(var i=0; i<=5;i++){
     setTimeout(() => {
       console.log(i);  // it will return 6 time 6, beacused i pointing same the same memory location and 2nd is global scope
     }, 3000);
}

/*----Example 4----*/


for(var i=0; i<=5;i++){
  function close(i){
  setTimeout(() => {
    console.log(i); // we need only value with var, so the we have create one more function it save fresh memory space
  }, 300);
}
close(i)
}

 // Note:  if you dont want to create close function  then and you can remove and use let instead var
 // OP: 1,2,3,4,5,


 function book(){
  let ds="Zarar Ali";
  function student(){
    console.log(ds);
  }
 return student // closuer word is return- closure will enclosed function along with lexical scope and book vanished from here and book no longer in call stack
}
let fvbook= book();
console.log(fvbook);
fvbook();






















