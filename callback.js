/* ---------------What is CALBACK----------------*/

/*
Callback is function which is call when task is completed.
1. It helps us to avaoid waiting time, and working as asnchronous. 
2. Node Js handle large number of request without waiting or it will not block the code using the callback

A callback is a function which is called when a task is completed, thus helps in preventing any kind of blocking and a callback function allows other code to run in the meantime. 
Callback is called when task get completed and is asynchronous equivalent for a function
=======================================
Example 1: Blocking (Waiting request until get response then it will  not move into next line)

var fs = require("fs");
var data = fs.readFileSync('input.txt'); 
console.log(data.toString());
console.log("Program Ended");

Note: Its a Blocking request. it will wait until get response of data and will not move into next line

=================================================================
Example 2: Non Blocking (Below one is call back function)

fs.readFile(data, function(err,data){
    if(err){
        return error.console(err);
    }else{
        return data.toString
    }
})by

 So it can be high number of request without waiting for any function to return result
*/

