/*
FCF: A programming language is said to have First-class 
functions when functions in that language are treated like any other variable
- We can pass a function inside a function just like a varibale.
*/

function sqr(num){
    return num*num
}
function disPlayFunction(fn){
    console.log("Total value is " + fn(5));
}

disPlayFunction(sqr);