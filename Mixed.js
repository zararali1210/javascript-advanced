/*
1. Primitive Data Type: Number, Boolean, String, null, undefined, 
2. Non Primitive Data Type:  Array, Object, Function, Date, Reg

3 Error First Callback:   First parm will err and second will data or response  in call function 


1. Asynchronous

The architecture of asynchronous explains that the message sent will not give the reply on immediate basis
 just like we send the mail but do not get the reply on an immediate basis. It does not have any dependency or order. 
 Hence improving the system efficiency and performance. The server stores the information and when the action is done it will be notified.

2. Non-Blocking

Nonblocking immediately responses with whatever data available. Moreover, it does not block any execution 
and keeps on running as per the requests. If an answer could not be retrieved then in those cases API returns immediately with an error.
Nonblocking is mostly used with I/O(input/output). Node.js is itself based on nonblocking I/O model. 
There are few ways of communication that a nonblocking I/O has completed. 
The callback function is to be called when the operation is completed. Nonblocking call uses the help of javascript which provides a 
callback function.


NoTE: PUT and PATCH ---
2. Reator Pattern
3. When use npm and yarn
4. REPL
*/

/*Slice:
1. Slice will return new array and just take the element from the array based on you mentioned
2. slice(Start index, new index-but not included)
3. It not changed the original array
4. slice() method returns the selected element(s) in an array
*/

let sliceArry=[1,3,5,6,9,12,56]
console.log(sliceArry.slice(-5,-3)); // In case start from left index 1 and form second param will be 0
console.log(sliceArry.slice(3,5)); //[6,9]
console.log(sliceArry.slice(4,6)); // [9,12]
console.log(sliceArry,'-------------')

/*Splice:
1. It will delete the record and add the element also
2. It changed the original array
3. The splice() method returns the removed item(s) in an array
*/
console.log(sliceArry.splice(1,3,"Hi")); //[1, "Hi",9,12,56]
console.log(sliceArry)

var array=[1,2,3,4,5];
console.log(array.slice(1)); // if we one then it will not included

const fruits = ["Banana", "Orange", "Apple", "Mango",1,2,3,5];
fruits.splice(3, 2, "Lemon", "Kiwi");
console.log(fruits)


const ent = ["Banana", "Orange", "Apple", "Mango"];
const f = ent.entries();

for(let i of f){
    console.log(i);
}

/*
What do you understand by the term I/O?
The term I/O stands for input and output. It is used to access anything outside of your application. The I/O describes any program, operation, or device that transfers data to or from a medium or another medium. This medium can be a physical device, network, or files within a system.

I/O is loaded into the machine memory to run the program once the application starts.

=============================================================================
What are the security mechanisms available in Node.js?
Using the Helmet module

Helmet helps to secure your Express applications by setting various HTTP headers, like:

X-Frame-Options to mitigates clickjacking attacks,
Strict-Transport-Security to keep your users on HTTPS,
X-XSS-Protection to prevent reflected XSS attacks,
X-DNS-Prefetch-Control to disable browsers DNS prefetching.
const express = require('express')
const helmet = require('helmet')
const app = express()

app.use(helmet())

==============================================================
What is a control flow function? What are the steps does it execute?
It is a generic piece of code which runs in between several asynchronous function calls is known as control flow function.

It executes the following steps.

Control the order of execution.
Collect data.
Limit concurrency.
Call the next step in the program.



*/