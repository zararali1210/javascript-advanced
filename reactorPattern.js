/*
Reactor Pattern in Node.js is basically a concept of non-blocking I/O operations. 
This pattern provides a handler that is associated with each I/O operation and as soon as an I/O request is generated, 
it is then submitted to a demultiplexer. This demultiplexer is a notification interface which is capable of handling concurrency 
in non-blocking I/O mode. It also helps in collecting each and every request in the form of an event and then place each event in a queue.
Thus resulting in the generation of the Event Queue. 
Simultaneously, we have our event loop which iterates the events present in the Event Queue.


Reactor Pattern is used to avoid the blocking of the Input/Output operations. It provides us with a handler
that is associated with I/O operations. When the I/O requests are to be generated, they get submitted to a demultiplexer,
which handles concurrency in avoiding the blocking of the I/O mode and collects the requests in form of an event and queues those events.

There are two ways in which I/O operations are performed:

Blocking I/O: Application will make a function call and pause its execution at a point until the data is received. It is called as ‘Synchronous’.
Non-Blocking I/O: Application will make a function call, and, without waiting for the results it continues its execution. It is called as ‘Asynchronous’.
*/