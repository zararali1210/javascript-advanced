/*---------------------Middleware--------------------------------*/

/*
1. Middleware handling the multiple routing of the webpages, It work between request and response
2. Middleware get executed after the server receive the request and before the controller action send the response
3. Middleware has access the request object, response object and next
4. It can process the request before sending the response from  server
5. Middleware can used to add login authentication and functionality
6. It used for client side rendering performance
7. Its use optimization and better performance

Syntex
app.get(path, (res,res,next)=>{},(req,res)=>{})

An Express application can use the following types of middleware:

1. Application-level middleware (app.get((----)))
2. Router-level middleware (router.get((---)))
3. Error-handling middleware
4. Built-in middleware
5. Third-party middleware (cookie-parse)


3. Error-handling middleware always takes four arguments. You must provide four arguments to identify it as an error-handling middleware function. Even if you don’t need to use the next object, you must specify it to maintain the signature.
Otherwise, the next object will be interpreted as regular middleware and will fail to handle errors.

4. Built-in middleware
Starting with version 4.x, Express no longer depends on Connect. The middleware functions that were previously included with Express are now in separate modules; see the list of middleware functions.

Express has the following built-in middleware functions:

express.static serves static assets such as HTML files, images, and so on.
express.json parses incoming requests with JSON payloads. NOTE: Available with Express 4.16.0+
express.urlencoded parses incoming requests with URL-encoded payloads. NOTE: Available with Express 4.16.0+

*/
