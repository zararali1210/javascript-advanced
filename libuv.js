/*
What is Libuv?
Libuv is a widely used library present in Node.js. It is used to complement the asynchronous I/O functionality of Node.js.
 It was developed in-house and used alongside systems such as Luvit, Julia, and more.

Following are some of the features of Libuv:

File system event handling
Child forking and handling
Asynchronous UDP and TCP sockets
Asynchronous file handling and operations
*/