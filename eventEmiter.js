 /*---------------Node Js Module and Event Emitter-------------------------*/
/*
Event module: Node js has a build-in module, called events
Where you can create, fire, listen for your own events.
1.  Registering for the event to be fired only time
2.  Register for the event to be fired with the multiple callback
3.  Register for the events to be fired with Callback parameters

*/
/*----------------Example 1---------------------*/

let EventsEmitter= require('events')
let event= new EventsEmitter();  // Create

event.on('myFunc',()=>{
    console.log("This is check once only"); // Listen
})

event.emit("myFunc") // Fire or emit

/*----------------Example 3---------------------*/

event.on('multipleCallback',()=>{
    console.log("This is check one time"); // Listen
})
event.on('multipleCallback',()=>{
    console.log("This is check 2nd time"); // Listen
})
event.on('multipleCallback',()=>{
    console.log("This is check 3rd time"); // Listen
})

event.emit('multipleCallback');


/*----------------Example 3---------------------*/

event.on('paramFunc',(statusCode, message)=>{
    console.log(`My status code is ${statusCode} and message is ${message}` )
})

event.emit("paramFunc", 200,"OK") // Fire or emit



