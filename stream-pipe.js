/*------------------------STREAM-------------------------------------*/

/*
Stream: Stream are the object that let you know the read or write the data from the source and pass to destination
We have 4 type Method
1. Readable-> Stream which is used for read operation
2. Writeable-> Stream which is used for write operation
3. Duplex-> stream which is read and write operation
4. Transform-> This type of stream where the computed output based on input

But some events are trigger

data-> This event is trigger where data are available for read
end-> This event is trigger where data are not available for read
error-> This event is trigger where if any error receiving while reading or writing the data
finish-> This event is fired where all the data has been flush to underlying system

*/


// const fs= require("fs");

// const fsStream= fs.createReadStream('index.html');
// fsStream.on('data',(data)=>{
//     console.log(data)
// })
// fsStream.on('end', ()=>{
//     console.log('end')
// })
// fsStream.on('err', (err,data)=>{
//     console.log(err)
// })

//Another Example with pipe

// Node.js program to demonstrate the    
// readable.pipe() method
  
// Accessing fs module
var fs = require("fs");
 
// Create a readable stream
var readable = fs.createReadStream('input.txt');
 
// Create a writable stream
var writable = fs.createWriteStream('output.txt');
 
// Calling pipe method
readable.pipe(writable);
 
console.log("Program Ended");

/*

I real time we can read the value and pass the data to destination via pipe


io.sockets.emit will send to all the clients

socket.broadcast.emit will send the message to all the other clients except the newly created connection

Note:

r  - open file for reading
r+ - open file for reading and writing
w  - open the file for writing, and created if does not exist or truncate if already exist
w+ - open the file for reading and writing, and created if does not exist or truncate if already exist
a  - open file for appending and created file if does not exist
a+ - open the file reading and apending and created if does not exist







Example:

var fs = require("fs");

// Asynchronous - Opening File
console.log("Going to open file!");
fs.open('input.txt', 'r+', function(err, fd) {
   if (err) {
      return console.error(err);
   }
   console.log("File opened successfully!");     
});




===========================================================================================================================

Chaining the Streams

Chaining is a mechanism to connect the output of one stream to another stream and create a chain of multiple stream operations. 
It is normally used with piping operations.

var fs = require("fs");
var zlib = require('zlib');

// Compress the file input.txt to input.txt.gz
fs.createReadStream('input.txt')
   .pipe(zlib.createGzip())
   .pipe(fs.createWriteStream('input.txt.gz'));
  
console.log("File Compressed.");

*/





